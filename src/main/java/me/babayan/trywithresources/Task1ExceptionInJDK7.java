package me.babayan.trywithresources;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Task1ExceptionInJDK7 {
    public static void main(String[] args) {

        /**
         * exception try(with resources)
         * */
        try (BufferedReader br = new BufferedReader(new FileReader("data/exception.txt"))) {
            String str;
            while ((str = br.readLine()) != null) {
                System.out.println(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}