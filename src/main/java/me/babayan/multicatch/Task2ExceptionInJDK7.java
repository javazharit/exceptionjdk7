package me.babayan.multicatch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Task2ExceptionInJDK7 {
    public static void main(String[] args) {
        int n;
        int res;

        /**
         * exception try(with resources) and multi catch
         * */
         try (BufferedReader br = new BufferedReader(new FileReader("data/exception.txt"))) {
            String str;
            while ((str = br.readLine()) != null) {
                System.out.println(str);
                n = Integer.parseInt(str);
                res = n / 0;
                System.out.println(res);
            }
             /**
              * multi catch
              * */
        } catch (ArithmeticException | IOException ex) {
            System.out.println("exception: " + ex);
        }
    }
}